import cv2, json, os, operator
import numpy as np
import matplotlib.pyplot as plt

def roi_saliency(imagefile, opath, r):
    f = os.path.join(opath, imagefile)
    im = cv2.imread(f)
    roi = im[int(r[1]):int(r[1]+r[3]), int(r[0]):int(r[0]+r[2])]
    roi_sum = np.sum(roi)
    im_sum = np.sum(im)
    return (roi_sum / im_sum).item(), roi_sum.item(), im_sum.item()

ipath = '~images'
opath = '~saliency'
if not os.path.exists(ipath):
    os.mkdir(ipath)
if not os.path.exists(opath):
    os.mkdir(opath)

(_, _, filenames) = next(os.walk(opath))
imagefiles = [i for i in filenames if i[-4:] == '.png']
print(len(imagefiles), 'images')

imgfile = os.path.join(opath, imagefiles[0])
im = cv2.imread(imgfile)

roifile = os.path.join(opath, 'roi.txt')
if not os.path.exists(roifile):
    ## Select ROI
    r = cv2.selectROI(im)
    with open(roifile, 'w') as file:
        file.write(json.dumps(r))
else:
    with open(roifile) as f:
        r = json.loads(f.read())
        im_crop = im[int(r[1]):int(r[1]+r[3]), int(r[0]):int(r[0]+r[2])]
        plt.imshow(im_crop)
        plt.show()

print('Region of Interest', r)

## Compute ROI saliency
details = {i:roi_saliency(i, opath, r) for i in imagefiles}
with open(os.path.join(opath,'roi_saliency_details.json'), 'w') as f:
    f.write(json.dumps(details, sort_keys=True, indent=4))

## Sort ROI saliency in descending order
saliency_list = [(k,v[0]) for k,v in details.items()]
saliency_list.sort(key=operator.itemgetter(1), reverse=True)
with open(os.path.join(opath,'roi_saliency.json'), 'w') as f:
    f.write(json.dumps({k:v for k,v in saliency_list}, indent=4))

# for k,v in saliency_list:
#     print(k, v)
print(*saliency_list)
