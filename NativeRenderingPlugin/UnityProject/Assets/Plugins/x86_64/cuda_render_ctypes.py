# -*- coding: utf-8 -*-
"""
Created on Sat Oct 27 10:59:10 2018

@author: hasee
"""

from PIL import Image
import ctypes, random, datetime, time, json, os, cv2, math, operator
import numpy as np
import transformations as tr
import matplotlib.pyplot as plt

class float4(ctypes.Structure):
        _fields_ = [
                ('r', ctypes.c_float),
                ('g', ctypes.c_float),
                ('b', ctypes.c_float),
                ('a', ctypes.c_float)]

        def __init__(self, t):
                self.r, self.g, self.b, self.a  = t

        def data(self):
                return [self.r, self.g, self.b, self.a]

class float3(ctypes.Structure):
        _fields_ = [
                ('x', ctypes.c_float),
                ('y', ctypes.c_float),
                ('z', ctypes.c_float)]

        def __init__(self, t):
                self.x, self.y, self.z = t

        def data(self):
                return [self.x, self.y, self.z]

def tf_point(intensity, rgba):
        return intensity, rgba.r, rgba.g, rgba.b, rgba.a

def save_image(img, ipath, id):
        filename = os.path.join(ipath, '{:03d}.png'.format(id))
        Image.fromarray(img).save(filename)

def save_transfer_functions(tfs, opath):
        tf_filename = os.path.join(opath, 'transfer_functions.json')
        names = ['{:03d}'.format(i) for i in range(len(tfs))]
        # tf_filename = os.path.join(tfpath, '{:03d}.json'.format(id))
        with open(tf_filename, 'w') as file:
                d = {name:tf.tolist() for tf,name in zip(tfs, names)}
                file.write(json.dumps(d, indent=4))

def save_saliency_list(st_list, opath):
        with open(os.path.join(opath,'roi_saliency.json'), 'w') as f:
                f.write(json.dumps({k:(v,a.tolist()) for k,v,a in st_list}, indent=4))

## generate random 16 bin color transfer functions
def draw_random_16(set_transfer_function_lerp, cuda_draw, image, N, tf_intensity, tf_rgba, modelview):
        for i in range(N):
                tf_intensity[i] = i / (N-1)
        for i in range(N):
                tf_rgba[i].r = random.uniform(0,1)
                tf_rgba[i].g = random.uniform(0,1)
                tf_rgba[i].b = random.uniform(0,1)
                tf_rgba[i].a = random.uniform(0,1)
        set_transfer_function_lerp(N, tf_intensity, tf_rgba)
        img3 = cuda_draw(modelview, image)
        flat_pixels3 = [(img3[i*4], img3[i*4+1], img3[i*4+2]) for i in range(width*height)]
        new_im3 = Image.new('RGB', (width, height))
        new_im3.putdata(flat_pixels3)
        return np.array(new_im3), [tf_point(tf_intensity[i],tf_rgba[i]) for i in range(N)]

## generate random 16 bin alpha transfer functions
def draw_random_16_alpha(set_transfer_function_lerp, cuda_draw, image, N, tf_intensity, tf16, modelview):
        mid = np.arange(0.5/N, 1, 1/N)
        for i in range(N):
                tf_intensity[i] = mid[i]
        for i in range(N):
                tf16[i].a = random.uniform(0,1)
        set_transfer_function_lerp(N, tf_intensity, tf16)
        img3 = cuda_draw(modelview, image)
        flat_pixels3 = [(img3[i*4], img3[i*4+1], img3[i*4+2]) for i in range(width*height)]
        new_im3 = Image.new('RGB', (width, height))
        new_im3.putdata(flat_pixels3)
        return np.array(new_im3), [tf_point(tf_intensity[i],tf16[i]) for i in range(N)]

## draw volume with the given transfer function
def draw_tf(set_transfer_function_lerp, cuda_draw, image, tf, modelview):
        N = len(tf)
        tf_intensity = (ctypes.c_float * N)()
        tf_rgba = (float4 * N)()
        for i in range(N):
                tf_intensity[i] = tf[i][0]
                tf_rgba[i].r = tf[i][1]
                tf_rgba[i].g = tf[i][2]
                tf_rgba[i].b = tf[i][3]
                tf_rgba[i].a = tf[i][4]
        set_transfer_function_lerp(N, tf_intensity, tf_rgba)
        img3 = cuda_draw(modelview, image)
        flat_pixels3 = [(img3[i*4], img3[i*4+1], img3[i*4+2]) for i in range(width*height)]
        new_im3 = Image.new('RGB', (width, height))
        new_im3.putdata(flat_pixels3)
        return np.array(new_im3), [tf_point(tf_intensity[i],tf_rgba[i]) for i in range(N)]

## generate random 256 bin alpha transfer functions
def draw_random_256_alpha(set_transfer_function_array, cuda_draw, image, BIN_COUNT, tf_array, modelview):
        for i in range(BIN_COUNT):
                tf_array[i].a = random.uniform(0,1)
        set_transfer_function_array(tf_array)
        img3 = cuda_draw(modelview, image)
        flat_pixels3 = [(img3[i*4], img3[i*4+1], img3[i*4+2]) for i in range(width*height)]
        new_im3 = Image.new('RGB', (width, height))
        new_im3.putdata(flat_pixels3)
        n = BIN_COUNT - 1
        return np.array(new_im3), [tf_point(i/n,tf_array[i]) for i in range(BIN_COUNT)]

def fine_grained(image):
    # initialize OpenCV's static fine grained saliency detector and
    # compute the saliency map
    saliency = cv2.saliency.StaticSaliencyFineGrained_create()
    (_, saliencyMap) = saliency.computeSaliency(image)
    return saliencyMap

def translate_rotate(translation, rotation, scale):
        # I = tr.identity_matrix()
        T = tr.translation_matrix(translation)
        R1 = tr.rotation_matrix(rotation[0],[1, 0, 0])
        R2 = tr.rotation_matrix(rotation[1],[0, 1, 0])
        R3 = tr.rotation_matrix(rotation[2],[0, 0, 1])
        S = tr.scale_matrix(scale)
        M = np.matmul(np.matmul(np.matmul(np.matmul(S, R1), R2), R3), T)
        return M.flatten(order='F')

def mv_array(mv):
        modelview = (ctypes.c_float * 16)()
        for i in range(16):
                modelview[i] = mv[i]
        return modelview

def unpack_image_tf(im_tf):
        unpack = [list(i) for i in zip(*im_tf)]
        return unpack[0], np.array(unpack[1])

def randomize_tf(tf):
        n = len(tf)
        r = np.random.rand(n)
        return np.array([*tf.T[:-1],r]).T

def edit_tf(tf, a):
        return np.array([*tf.T[:-1],a]).T

def roi_saliency(im, r):
    roi = im[int(r[1]):int(r[1]+r[3]), int(r[0]):int(r[0]+r[2])]
    roi_sum = np.sum(roi)
    im_sum = np.sum(im)
    return (roi_sum / im_sum).item(), roi_sum.item(), im_sum.item()

def mutate(l, x):
        n = len(l[0]) ## number of control points in the transfer function, e.g. 16
        r = np.random.rand(n)
        return [(1-x)*i + x*r for i in l]

def cross_over(l):
        r = np.random.rand(len(l))
        l2 = random.sample(l, k=len(l))
        return [(1-x)*a + x*b for a,b,x in zip(l, l2, r)]

## for genetic algorithm
def next_generation(top, spare):
        a = top + cross_over(top) + spare
        return a + mutate(a, 0.2)

def natural_select(a):
        # n = len(a) - 1
        # return [a[i] for i in range(len(a)) if random.uniform(0,1)**2>i/n]
        n = int(len(a)/6)
        top = a[:n]
        rest = a[n:]
        spare = random.sample(rest, k=n)
        return top, spare

## Genetic Algorithm
def genetic_algorithm(number_of_images, matrices, alpha_list, tf, set_transfer_function_lerp, cuda_draw, image, roi, ipath, opath):
        im_tf = [draw_tf(set_transfer_function_lerp, cuda_draw, image, edit_tf(tf,a), mv_array(m)) for a,m in zip(alpha_list,matrices)]
        imgs, tfs = unpack_image_tf(im_tf)
        alpha_list = [i.T[-1] for i in tfs]
        saliency_imgs = [fine_grained(i) for i in imgs]

        save_transfer_functions(tfs, opath)
        for i in range(len(imgs)):
                save_image(imgs[i], ipath, i)
                save_image(saliency_imgs[i], opath, i)

        names = ['{:03d}'.format(id) for id in range(number_of_images)]
        st_list = [(n,roi_saliency(i,roi)[0],a) for n,i,a in zip(names, saliency_imgs, alpha_list)]
        st_list.sort(key=operator.itemgetter(1), reverse=True)
        tf_list = [i[-1] for i in st_list]
        top, spare = natural_select(tf_list)
        new_alpha_list = next_generation(top, spare)

        for k, v, _ in st_list:
                print(k, v)
        save_saliency_list(st_list, opath)

        return new_alpha_list

## import functions from dynamic library
lib = ctypes.cdll.LoadLibrary('cuda_raycast')
cuda_draw = getattr(lib, 'cuda_draw')
cuda_draw.restype = ctypes.POINTER(ctypes.c_ubyte)
cuda_init = getattr(lib, 'cuda_init')
cuda_init.restype = ctypes.POINTER(ctypes.c_ubyte)
cuda_free = getattr(lib, 'cuda_free')
get_height = getattr(lib, 'get_height')
get_width = getattr(lib, 'get_width')
get_transfer_function_array = getattr(lib, 'get_transfer_function_array')
get_transfer_function_array.restype = ctypes.POINTER(float4)
set_transfer_function_array = getattr(lib, 'set_transfer_function_array')
set_transfer_function_lerp = getattr(lib, 'set_transfer_function_lerp')

translation = [0, 0, 4]
rotation = [0, 0, 0]
scale = 1
modelview = mv_array(translate_rotate(translation, rotation, scale))

img = cuda_init()
width = get_width()
height = get_height()
print('width =', width, 'height =', height)
flat_pixels = [(img[i*4], img[i*4+1], img[i*4+2]) for i in range(width*height)]
new_im = Image.new('RGB', (width, height))
new_im.putdata(flat_pixels)
new_im.save('cuda_init.png')

BIN_COUNT = 256
tf_array = get_transfer_function_array()
N = 16
tf_intensity = (ctypes.c_float * N)()
tf_rgba = (float4 * N)()
indices = list(zip(np.arange(0, BIN_COUNT+1-N, N), np.arange(N, BIN_COUNT+1, N)))
tf256 = np.array([tf_array[i].data() for i in range(BIN_COUNT)])
a16 = np.stack([np.mean(tf256[a:b], axis=0) for a,b in indices])
# print('a16',a16)
tf16 = (float4 * N)()
for i in range(N):
        tf16[i] = float4(a16[i].tolist())
mid = np.arange(0.5/N, 1, 1/N)
tf = np.array([mid,*a16.T]).T
# print(tf, tf.shape)
image = (ctypes.c_ubyte * width*height*4)()

COUNT = 6 ## number of generations to compute
## start Genetic Algorithm with initial population of 20
number_of_images = 24
## generate model view matrix with rotation
matrices = [translate_rotate([0,0,4],[i*math.pi/16,0,0],1) for i in range(number_of_images)]
## generate random alpha values for transfer functions
alpha_list = np.random.rand(number_of_images, N)

## Set a control point's alpha to 0 to have clear initial transfer functions
m = min(N, number_of_images)
print(m)
for i in range(m):
        alpha_list[i][i] = 0

roifile = 'roi.txt'
if not os.path.exists(roifile):
        ## Select ROI
        im = fine_grained(np.array(new_im))
        roi = cv2.selectROI(im)
        with open(roifile, 'w') as file:
                file.write(json.dumps(roi))
else:
        with open(roifile) as f:
                roi = json.loads(f.read())
print('Region of Interest', roi)

for no in range(COUNT):
        print('gneration', no)
        ipath = '~{:02d}'.format(no)
        opath = '~saliency' + ipath
        if not os.path.exists(ipath):
                os.mkdir(ipath)
        if not os.path.exists(opath):
                os.mkdir(opath)
        alpha_list = genetic_algorithm(number_of_images, matrices, alpha_list, tf, set_transfer_function_lerp, cuda_draw, image, roi, ipath, opath)

cuda_free()
