﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeMatrix : MonoBehaviour {
    public Matrix4x4 modelMatrix;

    // Use this for initialization
    void Start () {
        modelMatrix = transform.localToWorldMatrix;
    }
	
	// Update is called once per frame
	void Update () {
        modelMatrix = transform.localToWorldMatrix;
    }
}
