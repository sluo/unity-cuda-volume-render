﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMatrix : MonoBehaviour {
    public float translationSpeed = 1;
    public float rotationSpeed = 45;
    public Matrix4x4 viewMatrix;

    // Use this for initialization
    void Start () {
        viewMatrix = transform.worldToLocalMatrix;
    }
	
	// Update is called once per frame
	void Update () {
        var dy = Input.GetAxis("Horizontal") * rotationSpeed * Time.deltaTime; // Standard Left-/Right Arrows and A & D Keys
        transform.eulerAngles -= new Vector3(0, dy, 0);
        var dz = Input.GetAxis("Vertical") * translationSpeed * Time.deltaTime;
        transform.position += new Vector3(0, 0, dz);
        viewMatrix = transform.worldToLocalMatrix;
    }
}
