#pragma once

#ifndef CALLBACK_H
#define CALLBACK_H

#include <iostream>
#include <fstream>
#include <string>

#ifndef EXPORT
#define EXPORT extern "C" __declspec(dllexport)
#endif

typedef void(__stdcall * Callback) (const char * str);
Callback _callback = NULL;

EXPORT void setup_callback(Callback callback)
{
	if (callback)
	{
		_callback = callback;
	}
}

void unity_log(std::string message)
{
	if (_callback)
	{
		_callback(message.c_str());
	}
}

#endif // CALLBACK_H
