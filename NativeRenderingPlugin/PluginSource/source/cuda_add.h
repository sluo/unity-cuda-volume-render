#pragma once

#ifdef BARNABY_EXPORTS
#define BARNABY_API extern "C" __declspec(dllexport)
#else
#define BARNABY_API extern "C" __declspec(dllimport)
#endif

#ifndef CUDA_ADD_H
#define CUDA_ADD_H

BARNABY_API unsigned char * cuda_init();
BARNABY_API void cuda_free();
BARNABY_API unsigned int get_height();
BARNABY_API unsigned int get_width();
BARNABY_API unsigned char * cuda_draw(const float model_view[16] = NULL, unsigned char *target = NULL);

#endif // CUDA_ADD_H
