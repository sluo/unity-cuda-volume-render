#pragma once

#ifndef Matrix4x4_H
#define Matrix4x4_H

#include <iomanip>

/// https://github.com/Unity-Technologies/UnityCsReference/blob/master/Runtime/Export/Matrix4x4.cs
struct Matrix4x4
{
	float m00;
	float m10;
	float m20;
	float m30;
	float m01;
	float m11;
	float m21;
	float m31;
	float m02;
	float m12;
	float m22;
	float m32;
	float m03;
	float m13;
	float m23;
	float m33;

	Matrix4x4()
	{
		m00 = 1;
		m10 = 0;
		m20 = 0;
		m30 = 0;

		m01 = 0;
		m11 = 1;
		m21 = 0;
		m31 = 0;

		m02 = 0;
		m12 = 0;
		m22 = 1;
		m32 = 0;
		
		m03 = 0;
		m13 = 0;
		m23 = 0;
		m33 = 1;
	}
};

// Multiplies two matrices.
inline Matrix4x4 multiply(Matrix4x4 lhs, Matrix4x4 rhs)
{
	Matrix4x4 res;
	res.m00 = lhs.m00 * rhs.m00 + lhs.m01 * rhs.m10 + lhs.m02 * rhs.m20 + lhs.m03 * rhs.m30;
	res.m01 = lhs.m00 * rhs.m01 + lhs.m01 * rhs.m11 + lhs.m02 * rhs.m21 + lhs.m03 * rhs.m31;
	res.m02 = lhs.m00 * rhs.m02 + lhs.m01 * rhs.m12 + lhs.m02 * rhs.m22 + lhs.m03 * rhs.m32;
	res.m03 = lhs.m00 * rhs.m03 + lhs.m01 * rhs.m13 + lhs.m02 * rhs.m23 + lhs.m03 * rhs.m33;

	res.m10 = lhs.m10 * rhs.m00 + lhs.m11 * rhs.m10 + lhs.m12 * rhs.m20 + lhs.m13 * rhs.m30;
	res.m11 = lhs.m10 * rhs.m01 + lhs.m11 * rhs.m11 + lhs.m12 * rhs.m21 + lhs.m13 * rhs.m31;
	res.m12 = lhs.m10 * rhs.m02 + lhs.m11 * rhs.m12 + lhs.m12 * rhs.m22 + lhs.m13 * rhs.m32;
	res.m13 = lhs.m10 * rhs.m03 + lhs.m11 * rhs.m13 + lhs.m12 * rhs.m23 + lhs.m13 * rhs.m33;

	res.m20 = lhs.m20 * rhs.m00 + lhs.m21 * rhs.m10 + lhs.m22 * rhs.m20 + lhs.m23 * rhs.m30;
	res.m21 = lhs.m20 * rhs.m01 + lhs.m21 * rhs.m11 + lhs.m22 * rhs.m21 + lhs.m23 * rhs.m31;
	res.m22 = lhs.m20 * rhs.m02 + lhs.m21 * rhs.m12 + lhs.m22 * rhs.m22 + lhs.m23 * rhs.m32;
	res.m23 = lhs.m20 * rhs.m03 + lhs.m21 * rhs.m13 + lhs.m22 * rhs.m23 + lhs.m23 * rhs.m33;

	res.m30 = lhs.m30 * rhs.m00 + lhs.m31 * rhs.m10 + lhs.m32 * rhs.m20 + lhs.m33 * rhs.m30;
	res.m31 = lhs.m30 * rhs.m01 + lhs.m31 * rhs.m11 + lhs.m32 * rhs.m21 + lhs.m33 * rhs.m31;
	res.m32 = lhs.m30 * rhs.m02 + lhs.m31 * rhs.m12 + lhs.m32 * rhs.m22 + lhs.m33 * rhs.m32;
	res.m33 = lhs.m30 * rhs.m03 + lhs.m31 * rhs.m13 + lhs.m32 * rhs.m23 + lhs.m33 * rhs.m33;

	return res;
}

Matrix4x4 g_view, g_projection, g_model;

#define EXPORT __declspec(dllexport)

extern "C" void EXPORT SetView(Matrix4x4 matrix)
{
	g_view = matrix;
}

extern "C" void EXPORT SetProjection(Matrix4x4 matrix)
{
	g_projection = matrix;
}

extern "C" void EXPORT SetWorld(Matrix4x4 matrix)
{
	g_model = matrix;
}

inline Matrix4x4 model_view_matrix()
{
	return multiply(g_model, g_view);
}

inline Matrix4x4 model_view_projection_matrix()
{
	return multiply(multiply(g_model, g_view), g_projection);
}

#ifndef LOG_FILE
#define LOG_FILE "RenderingPlugin.txt"
#endif

inline void print(const Matrix4x4 &m)
{
	// print matrix in row-major order
	float a[16] = {
		m.m00,m.m01,m.m02,m.m03,
		m.m10,m.m11,m.m12,m.m13,
		m.m20,m.m21,m.m22,m.m23,
		m.m30,m.m31,m.m32,m.m33
	};
	std::ofstream txt(LOG_FILE, std::ofstream::out | std::ofstream::app);
	txt << std::fixed << std::setprecision(9) << std::endl;
	txt << "{{" << a[0] << ", " << a[1] << ", " << a[2] << ", " << a[3] << "}," << std::endl;
	txt << " {" << a[4] << ", " << a[5] << ", " << a[6] << ", " << a[7] << "}," << std::endl;
	txt << " {" << a[8] << ", " << a[9] << ", " << a[10] << ", " << a[11] << "}," << std::endl;
	txt << " {" << a[12] << ", " << a[13] << ", " << a[14] << ", " << a[15] << "}}" << std::endl;
	txt << std::endl;
	txt.close();
}

#endif // Matrix4x4_H
