#pragma once

#ifndef DLL_LOG_H
#define DLL_LOG_H

#include <iostream>
#include <fstream>
#include <string>

const std::string log_file = "RenderingPlugin.txt";

void clear_dll_log(std::string s = "")
{
	std::ofstream txt(log_file);
	if (!s.empty())
	{
		txt << s << std::endl;
	}
	txt.close();
}

void dll_log(std::string message)
{
	std::ofstream txt(log_file, std::ofstream::out | std::ofstream::app);
	txt << message << std::endl;
	txt.close();
}

#endif // DLL_LOG_H
