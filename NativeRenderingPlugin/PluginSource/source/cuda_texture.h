#pragma once

#ifndef CUDA_TEXTURE_H
#define CUDA_TEXTURE_H

#include "cuda_add.h"

#ifndef EXPORT
#define EXPORT extern "C" __declspec(dllexport)
#endif

EXPORT unsigned int get_cuda_texture_height()
{
	return get_height();
}

EXPORT unsigned int get_cuda_texture_width()
{
	return get_width();
}

#endif // CUDA_TEXTURE_H
