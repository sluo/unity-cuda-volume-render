#pragma once

#ifndef SIZE_H
#define SIZE_H

#include <fstream>
#include <sstream>
#include <cuda_runtime.h>
#include "cereal/archives/json.hpp"
#include "dll_def.h"

struct Size
{
	size_t width, height, depth;

	Size()
	{
		width = height = depth = 0;
	}

	Size(cudaExtent e)
	{
		width = e.width;
		height = e.height;
		depth = e.depth;
	}

	cudaExtent data()
	{
		return make_cudaExtent(width, height, depth);
	}

	std::string str()
	{
		std::stringstream ss;
		ss << width << std::ends << height << std::ends << depth;
		return ss.str();
	}

	// This method lets cereal know which data members to serialize
	template<class Archive>
	void serialize(Archive & archive)
	{
		archive(CEREAL_NVP(width), CEREAL_NVP(height), CEREAL_NVP(depth)); // serialize things by passing them to the archive
	}
};

#endif // SIZE_H
