#pragma once

#ifndef DLL_DEF_H
#define DLL_DEF_H

#define BARNABY_EXPORTS

#ifdef BARNABY_EXPORTS
//#define BARNABY_API __declspec(dllexport)
#define BARNABY_API extern "C" __declspec(dllexport)
#else
#define BARNABY_API extern "C" __declspec(dllimport)
#endif

#endif // DLL_DEF_H
